#!/bin/sh
set -e

if [ "$(echo "$1" | cut -c1)" = "-" ]; then
  echo "$0: assuming arguments for dashd"

  set -- dashd "$@"
fi

echo
exec "$@"
